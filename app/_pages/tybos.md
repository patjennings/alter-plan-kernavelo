---
layout: page
title: Ty Bos
release-date: 2019
permalink: /ty-bos
excerpt_separator: <!--more-->
---

Pont sur l'avenue de Ty Bos

Le passage,
Un grand nombre de cyclistes pourrait arriver par là (depuis Fouesnant et Saint-Évarzec). Hors, le passage au dessus de la N??? est dangereux. Dans un sens, une étroite bande cyclable. Dans l'autre, aucun aménagement. Tout le long de cette avenue, l'étroite bande constitue un bien maigre aménagement : la cohabitation avec les nombreuses automobiles empruntant cette rue est difficile.

Des aménagements plus sérieux, et une passerelle permettant d'emjamber les quatres voies de l'avenue du Morbihan pour arriver, depuis le sud-est, avenue Georges Pompidou, permettraient d'assurer la sécurité des potentiels nombreux cyclistes empruntant cet axe.

<img src="assets/images/tybos_1.png" width="100%"/>

<img src="assets/images/tybos_2.jpg" width="100%"/>

Cela pourrait même, au passage, remettre en lumière l'ancienne connexion entre cette route venant de Concarneau, et le bourg d'Ergué-Armel

<img src="assets/images/tybos_historique.jpg" width="100%"/>

> **Sources**
>
> [L’agglo investit 4 M€ pour relier Plescop au centre de Vannes à vélo (Le Télégramme - 20 octobre 2022)](https://www.letelegramme.fr/morbihan/vannes/l-agglo-investit-4-meur-pour-relier-plescop-au-centre-de-vannes-a-velo-20-10-2022-13204461.php)
