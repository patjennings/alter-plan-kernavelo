---
layout: page
title: Frugy
release-date: 2019
permalink: /frugy
excerpt_separator: <!--more-->
---
Le Frugy est un élément naturel central à Quimper. Des solutions existent pour le contourner. Et certaines pourraient même permettre de l'emprunter

<img src="assets/images/frugy_croquis.png" width="100%"/>



### Exemple ###

[L'Allée à Cri-Cri](https://www.au5v.fr/L-allee-a-CriCri-une-realisation-exemplaire-pour-relier-un-quartier-et-le-coeur.html)

<img src="assets/images/frugy_1.png" width="100%"/>

En comparant les topologies de Creil et de Quimper, on remarque que la configuration y est similaire. On peut voir naître, sur tout ou partie du Mont, un aménagement semblable à celui de la passerelle à Cri-Cri, à Creil.

<img src="assets/images/frugy_schema.jpg" width="100%"/>

