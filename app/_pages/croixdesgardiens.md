---
layout: page
title: Croix des Gardiens
release-date: 2019
permalink: /croix-des-gardiens
excerpt_separator: <!--more-->
---
<img src="assets/images/croix_gardiens_croquis.png" width="100%"/>



<img src="assets/images/croix_gardiens_2.png" width="100%"/>

Analyse de l'emprise d'un RP hollandais, dans la configuration du RP de la Croix des Gardiens

<img src="assets/images/croix_gardiens_1.png" width="100%"/>
