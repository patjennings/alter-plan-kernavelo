---
layout: page
title: Eau Blanche
release-date: 2019
permalink: /eau-blanche
excerpt_separator: <!--more-->
---
Le secteur de l'Eau Blanche va subir de profondes mutations dans les années à venir. C'est l'occasion d'y intégrer des aménagements permettant de passer, dans le sens Ouest > Est, le mur de vitesse, jalonné par les rond-points de l'Eau Blanche et Philippe Lebon.

Un carrefour "à la hollandaise" sur le rond-point Philippe Lebon, permettrait d'assurer la connexion entre le centre-ville et le Rouillen. De même, depuis l'Eau Blanche, on peut se diriger vers le rdon-point de Tréqueffelec, assurer une connexion avec Cuzon et arriver, à Tréqueffelec, aux portes de Kerfeunteun.

<img src="assets/images/rp_lebon.jpg" width="100%"/>

Le passage vers ouest-est, et à plus forte raison le long de l'axe Allende-Eau Blanche-Bd d'Ergué-Armel, est rendu difficile par le grand nombre de voies et la vitesse du traffic motorisé. Pourtant, l'espace sur la chaussée est là : les mobilités douces devraient en bénéficier.

<img src="assets/images/rp_est_1.jpg" width="100%"/>


