---
layout: page
title: Halage
release-date: 2019
permalink: /halage
excerpt_separator: <!--more-->
---
Le chemin de halage constitue un lien évident entre le centre-ville et l'ouest, vers Penhars, Kerlagatu, le Corniguel et, plus loin, Plomelin et le Pays Bigouden. Il est très prisé des promeneurs, et il semble important de l'aménager afin que piétons et cyclistes cohabitent en toute sécurité. On voit, à travers les exemples présentés ici, que des solutions existent pour exploiter les berges de l'Odet à cet endroit

<img src="assets/images/halage_croquis.png" width="100%"/>


### Exemples ###

Aménagement des berges du Têt à Perpignan
<img src="assets/images/halage_1.png" width="100%"/>

<img src="assets/images/halage_2.png" width="100%"/>

<img src="assets/images/halage_3.png" width="100%"/>

<img src="assets/images/halage_4.png" width="100%"/>

