---
layout: page
title: Locmaria
release-date: 2022
permalink: /locmaria
excerpt_separator: <!--more-->
---

<img src="assets/images/locmaria_croquis.png" width="100%"/>

### Exemples ###

<img src="assets/images/locmaria_1.png" width="100%"/>

[La place Roz](https://www.europan-europe.eu/fr/exchanges/la-place-roz)

<img src="assets/images/locmaria_2.png" width="100%"/>

[Dour, Koad, Ker](https://www.europan-europe.eu/fr/exchanges/dour-koad-ker)

<img src="assets/images/locmaria_3.png" width="100%"/>

[Terre Glaz](https://www.europan-europe.eu/fr/exchanges/terre-glaz)
