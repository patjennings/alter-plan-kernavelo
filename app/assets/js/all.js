// L.mapbox.accessToken = 'pk.eyJ1IjoicGF0amVubmluZ3MiLCJhIjoiY2lqZndzdWNrMDAwOHZsbHczbmIxcTZsYyJ9.tAdDcTMS13Ggznt9HOGzFw';

(function(window){
    init();
})(window);

// const info = document.getElementById('info');
// info.style.pointerEvents = 'none';
// const tooltip = new Tooltip(info, {
//     animation: false,
//     customClass: 'pe-none',
//     offset: [0, 5],
//     title: '-',
//     trigger: 'manual',
// });

const info = document.getElementById('info');

function init(){
    // createMap();
    createMapboxMap();
    // var map = L.map('map').setView([39.74739, -105], 13);

    // initTooltips();
   
}

function createMapboxMap(){
    mapboxgl.accessToken = 'pk.eyJ1IjoicGF0amVubmluZ3MiLCJhIjoiY2lqZndzdWNrMDAwOHZsbHczbmIxcTZsYyJ9.tAdDcTMS13Ggznt9HOGzFw';
    const map = new mapboxgl.Map({
	container: 'map',
	// Choose from Mapbox's core styles, or make your own style with Mapbox Studio
	style: 'mapbox://styles/patjennings/cksehhcdrjlte18nxmscrj1b0',
	center: [-4.1016, 47.9942],
	zoom: 13
    });

    // console.log(points);

    map.on('load', () => {
	map.addSource('routes', {
	    'type': 'geojson',
	    'data': lines
	});
	map.addSource('points', {
	    'type': 'geojson',
	    'data': points
	});

	
	map.addLayer({
	    'id': 'route',
	    'type': 'line',
	    'source': 'routes',
	    'layout': {
		'line-join': 'round',
		'line-cap': 'round'
	    },
	    'paint': {
		'line-color': '#888',
		'line-width': 8,
		'line-opacity': 0.5
	    }
	});
	map.addLayer({
	    'id': 'point',
	    'type': 'circle',
	    'source': 'points',
	    'paint': {
		'circle-color': '#4264fb',
		'circle-radius': 8,
		'circle-stroke-width': 2,
		'circle-stroke-color': '#ffffff'
	    }
	});


	// mouse events
	// Center the map on the coordinates of any clicked circle from the 'circle' layer.
	map.on('click', 'point', (e) => {
	    // console.log(e.features[0].properties.url);
	    window.location.replace(e.features[0].properties.url);
	});
	
	// Change the cursor to a pointer when the it enters a feature in the 'circle' layer.
	map.on('mouseenter', 'point', (e) => {
	    map.getCanvas().style.cursor = 'pointer';
	    // console.log(e.features[0].properties.description);
	    // console.log("mouse position");
	    // console.log(e.point.x);
	    // console.log(e.point.y);
	    info.innerHTML = e.features[0].properties.name;
	    info.classList.remove('hidden');
	    info.classList.add('visible');
	    info.setAttribute('style', 'left: '+(e.point.x+20)+'px; top: '+(e.point.y-20)+'px;');
	    // console.log(info);

	    // afficher l'élément et le positionner
	    // le remplir avec le bon texte
	});
	
	// Change it back to a pointer when it leaves.
	map.on('mouseleave', 'point', () => {
	    map.getCanvas().style.cursor = '';
	    info.classList.add('hidden');
	    info.classList.remove('visible');
	});
    });
}
/*
function createMap(){


    // conteneur
    var map = L.map('map').setView([47.9942, -4.1016], 13);

    // fond de carte
    L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/{z}/{x}/{y}?access_token='+L.mapbox.accessToken, {
	maxZoom: 19,
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);


// styleUrl: "mapbox://styles/patjennings/cksehhcdrjlte18nxmscrj1b0",

// 		accessToken:

// 		"pk.eyJ1IjoicGF0amVubmluZ3MiLCJhIjoiY2lqZndzdWNrMDAwOHZsbHczbmIxcTZsYyJ9.tAdDcTMS13Ggznt9HOGzFw",

    
    // routes
    var linesStyle = {
	"Color": "#ff7800",
	"weight": 5,
	"opacity": 0.65
    }
    L.geoJSON(lines, {
	style: linesStyle
    }).addTo(map)

    // points
    // var pointsStyle = {
    // 	"Color": "#ff0000",
    // 	"weight": 5,
    // 	"opacity": 0.65
    // }

    var geojsonMarkerOptions = {
	radius: 8,
	fillColor: "#ff7800",
	color: "#ff7800",
	weight: 1,
	opacity: 1,
	fillOpacity: 0.8
    };
    function onEachFeature(feature, layer) {
	// does this feature have a property named popupContent?
	if (feature.properties && feature.properties.popupContent) {
            layer.bindPopup(feature.properties.popupContent);
	}
	layer.on('click', onMarkerClick)
	// layer.on('mouseOver', onMarkerHover)
	addPopup(feature, layer);
    }
    function addPopup(feature, layer) {
	var popupContent = feature.properties.name;
	layer.bindPopup(popupContent);

	layer.on('mouseover', function (e) {
            this.openPopup(e.latlng);
	});
	layer.on('mouseout', function (e) {
            this.closePopup();
	});
    }
    
    L.geoJSON(points, {
	// style: pointsStyle,
	pointToLayer: function (feature, latlng) {
            return L.circleMarker(latlng, geojsonMarkerOptions); // The basic style
	},
	onEachFeature: onEachFeature
	// onEachFeature: addPopup
    }).addTo(map)

    // map.on('click', onMapClick);

}

function onMarkerClick(e) {
    // console.log("You clicked the map at " + e.latlng);
    // console.log(e.target.feature.properties.name);
    window.location.replace(e.target.feature.properties.url);
}
*/

// const map = new Map({
// 	target: "map",
// 	layers: [
// 	    new MapboxVector({
// 		styleUrl: "mapbox://styles/patjennings/cksehhcdrjlte18nxmscrj1b0",
// 		accessToken:
// 		"pk.eyJ1IjoicGF0amVubmluZ3MiLCJhIjoiY2lqZndzdWNrMDAwOHZsbHczbmIxcTZsYyJ9.tAdDcTMS13Ggznt9HOGzFw",
// 	    }),
// 	    tracksLayer,
// 	    pointsLayer
// 	],
// 	view: new View({
// 	    center: WMTransform([-4.0802, 47.9933]),
// 	    zoom: 12.7,
// 	}),
// 	style: {
// 	    cursor: "" 
// 	}
//     });
